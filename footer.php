<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
<script src="vendor/trumbowyg/dist/trumbowyg.min.js"></script>
<script src="resources/js/main.js"></script>
<!-- Trumbowyg plugins -->
<script src="vendor/trumbowyg/dist/plugins/base64/trumbowyg.base64.min.js"></script>
<script src="vendor/trumbowyg/dist/plugins/preformatted/trumbowyg.preformatted.min.js"></script>
<script src="//rawcdn.githack.com/RickStrahl/jquery-resizable/0.35/dist/jquery-resizable.min.js"></script>
<script src="vendor/trumbowyg/dist/plugins/resizimg/trumbowyg.resizimg.min.js"></script>