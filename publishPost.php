<!DOCTYPE html>
<?php
require_once('./core/controllerPost.php');
$pc = new postController;
?>
<html>
<head>
<?php	require_once('./header.php'); ?>
	<meta charset="utf-8">
	<title>netz98 | New Post</title>
</head>
<body>

	<div class="container">
		<div class="row py-5">
		  <div class="col-12 text-center">
		    <h1 class="display-4 fw-bold">Publish new post</h1>
		  </div>
		  <?php if (isset($_SESSION['flashMessage'])): ?>
			  <div class="alert <?=$_SESSION['flashMessage']['className']?>" role="alert">
			    <?=$_SESSION['flashMessage']['msg']?>
			  </div>
			  <?php unset($_SESSION['flashMessage']) ?>
		  <?php endif ?>
		</div>
	</div>



	<div class="container">
		<div class="row">
		  <div class="col-12">

		  	<form method="POST" id="publishPost" enctype="multipart/form-data">
		  		<input hidden type="text" name="action" value="publish">
			    <div class="mb-3">
			      <label for="title" class="form-label lead">Title</label>
			      <input required type="text" class="form-control" id="title" name="title">
			    </div>

			    <div class="row">
			      <div class="col-2">
					    
					  </div>
					</div>

			    <div class="mb-3">
			      <div id="trumbowyg"></div>
			      <input hidden type="text" name="content" id="content">
			    </div>

			    <div class="mb-3">
			      <label class="form-label lead" for="featuredImage" name="featuredImage">Featured Image</label>
			      <input type="file" class="form-control" name="featuredImage" id="featuredImage">
			    </div>

			    <div class="col-12 d-flex justify-content-between">
			      <a href="index.php" class="btn btn-lg btn-secondary">Home</a>
			      <a href="allPosts.php" class="btn btn-lg btn-primary">All posts</a>
			      <button onclick="publishPostSubmit()" type="button" class="btn btn-lg btn-primary">Publish</button>
			    </div>
		    </form>

		  </div>
		</div>
	</div>
<?php	require_once('./footer.php'); ?>
<script>
$('#trumbowyg')
.trumbowyg({
    btnsDef: {
        image: {
            dropdown: ['insertImage', 'base64'],
            ico: 'insertImage'
        }
    },
    btns: [
        ['viewHTML'],
        ['formatting'],
        ['strong', 'em', 'del'],
        ['superscript', 'subscript'],
        ['link'],
        ['preformatted'],
        ['image'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['unorderedList', 'orderedList'],
        ['horizontalRule'],
        ['removeformat'],
        ['fullscreen']
    ],
    plugins: {
        resizimg: {
            minSize: 64,
            step: 16,
        }
    }
});

function publishPostSubmit() {
	let form = document.getElementById("publishPost");    
	if (form.checkValidity()) {
	  $('#content').val( $('#trumbowyg').trumbowyg('html') );
	  $('#publishPost').submit();
	} else {
	  form.reportValidity();
	}
}


</script>
</body>
</html>