<!DOCTYPE html>
<?php
require_once('./core/controllerPost.php');
$pc = new postController;
$page = $_GET['page'] ?? 1;
$data = $pc->postPagination($page);
if ($data['error404']) {
	die('Error 404. Page not found.');
}
?>
<html>
<head>
<?php	require_once('./header.php'); ?>
	<meta charset="utf-8">
	<title>netz98 | All Posts</title>
</head>
<body>

	<div class="container">
		<div class="row py-5">
		  <div class="col-12 text-center">
		    <h1 class="display-4 fw-bold">All Posts</h1>
		  </div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-12 d-flex justify-content-between">
			  <a href="index.php" class="btn btn-lg btn-secondary">Home</a>
			  <a href="publishPost.php" class="btn btn-lg btn-primary">New post</a>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">

			<?php foreach ($data['posts'] as $p): ?>
				   	 <div class="col-4 g-4">
					    <div class="card">
					      <img src="resources/uploads/<?=$p['featuredImage']?>" class="card-img-top" alt="...">
					      <div class="card-body">
					        <h5 class="card-title"><?=$p['title']?></h5>
					        <div>
						        <p class="card-text"><?=substr(strip_tags($p['content']),0, 100).'...'?></p>
						        <p class="font-weight-light">Posted: <u><?=date("F j, Y", strtotime($p['created']))?></u></p>
					        </div>
					        <a href="readPost.php?id=<?=$p['id']?>" class="btn btn-primary">Read more</a>
					      </div>
					    </div>
					   </div>
			<?php endforeach ?>

		</div>
	</div>

	<div class="container py-5">
		<div class="row">
			<nav>
			  <ul class="pagination justify-content-center">
			    <li class="page-item <?php if(empty($data['prevPage'])){echo 'disabled';} ?>">
			      <a class="page-link" href="allPosts.php?page=<?=$data['prevPage']?>">Previous</a>
			    </li>
			    <li class="page-item <?php if(empty($data['prevPage'])){echo 'd-none';} ?>">
			    	<a class="page-link" href="allPosts.php?page=<?=$data['prevPage']?>"><?=$data['prevPage']?></a>
			    </li>
			    <li class="page-item active" aria-current="page">
			      <a class="page-link" href="allPosts.php?page=<?=$data['page']?>"><?=$data['page']?></a>
			    </li>
			    <li class="page-item <?php if(empty($data['nextPage'])){echo 'd-none';} ?>">
			    	<a class="page-link" href="allPosts.php?page=<?=$data['nextPage']?>"><?=$data['nextPage']?></a>
			    </li>
			    <li class="page-item <?php if(empty($data['nextPage'])){echo 'disabled';} ?>">
			      <a class="page-link" href="allPosts.php?page=<?=$data['nextPage']?>">Next</a>
			    </li>
			  </ul>
			</nav>
		</div>
	</div>
<?php	require_once('./footer.php'); ?>
</body>
</html>