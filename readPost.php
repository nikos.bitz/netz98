<!DOCTYPE html>
<?php
require_once('./core/controllerPost.php');
$pc = new postController;
$id = $_GET['id'] ?? die('Error 404. Page not found.');
$post = $pc->getPost($id);
if( !$post ) {
	die('Error 204. Post not found.');
}

?>
<html>
<head>
<?php	require_once('./header.php'); ?>
	<meta charset="utf-8">
	<title>netz98 | <?=$post['title']?></title>
</head>
<body>

	<div class="container">
		<div class="row py-5">
		  <div class="col-12 text-center">
		    <h1 class="display-4 fw-bold"><?=$post['title']?></h1>
		  </div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-12 d-flex justify-content-between">
			  <a href="allPosts.php" class="btn btn-lg btn-secondary">All posts</a>
			  <a href="publishPost.php" class="btn btn-lg btn-primary">New post</a>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">

			<?=$post['content']?>

		</div>
	</div>

<?php	require_once('./footer.php'); ?>
</body>
</html>