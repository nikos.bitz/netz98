<pre>
netz98/
+--core/
	|--ajaxPost.php			-(Recieve ajax calls)
	|--controllerPost.php		-(Controller for Post pages, Connect frontend with backend model)	
	|--dbconnection.php		-(PDO class - database connection)
	|--importer.php			-(Class for importing data from netz98 wordpress website)
	|--modelPost.php		-(CRUD posts model - get-set data to database)
+--recources/
	|--uploads/			-(folder for store images)
+--vendor/
	|-- ...
--allPosts.php				-(Display all posts)
--footer.php				-(js scripts)
--header.php				-(css scripts)
--index.php				-(just index)
--publishPost.php			-(Publish custom posts)
--readPost.php				-(Display single post)
</pre>
