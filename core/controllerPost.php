<?php

require_once('modelPost.php');

/**
 * All backend procedures
 */
class postController
{
	private $modelPost;

	function __construct() {
		$this->modelPost = new postModel;
		if( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
			if ( isset($_POST['action']) AND $_POST['action'] == 'publish' ) {
				$this->postPublish();
			}
		}
	}

	public function postPagination($page) {
		return $this->modelPost->postPagination($page);
	}


	public function getPost($id=false) {
		return $this->modelPost->readById($id);
	}

	public function postPublish() {

		$arg = array(
			'title' => $_POST['title'] ? $_POST['title'] : null,
			'content' => $_POST['content'] ? $_POST['content'] : null
		);

		if ( empty($_FILES["featuredImage"]["name"]) ) {
			$arg['featuredImage'] = null;
		} else {
			$arg['featuredImage'] = $this->uploadImage();
			if (!$arg['featuredImage']) {
				return false;
			}
		}

		if ( $this->modelPost->create($arg) ) {
			$_SESSION['flashMessage'] = array(
				'msg' => 'Your post has been successfully published',
				'className' => 'alert-success'
			);
		} else {
			$_SESSION['flashMessage'] = array(
				'msg' => 'An error occurred while publishing your post',
				'className' => 'alert-danger'
			);
		}
	}

	private function uploadImage() {
		$target_dir = dirname(__FILE__, 2).'/resources/uploads/';
		$fileName = time().$_FILES["featuredImage"]["name"];
		$target_file = $target_dir . basename($fileName);
		$check = getimagesize($_FILES["featuredImage"]["tmp_name"]);
		move_uploaded_file($_FILES["featuredImage"]["tmp_name"], $target_file);
		if($check !== false) {
		  return $fileName;
		} else {
		  return false;
		}
	}

}


?>