<?php
require_once('modelPost.php');
require_once('controllerPost.php');

/**
 * Import posts from https://dev98.de
 */
class postImporter
{
	private $modelPost;

	function __construct() {
		$this->modelPost = new postModel;
	}

	public function getPostsFromDev98() {

		$dom = new DOMDocument();
		$links = $this->getPostsLinks();
		foreach ($links as $link) {

			$urlExist = @file_get_contents($link);
			$title = false;
			$image = false;
			$content = false;

			if( $urlExist ) {

				$html = file_get_contents($link);
				@$dom->loadHTML($html);

				// Get Title
				$h1 = $dom->getElementsByTagName('h1');
				for ($i = 0; $i < $h1->length; $i++) {
					if ( $h1->item($i)->getAttribute('class') == 'entry-title' ) {
						$title = $h1->item($i)->nodeValue;
						$post = $this->modelPost->readByTitle($title);
						if ($post) {
							continue 2;
						}
					}
				}

				// Get Image
				$img = $dom->getElementsByTagName('img');
				for ($i = 0; $i < $img->length; $i++) {
					if ( $img->item($i)->getAttribute('class') == 'attachment-nisarg-full-width size-nisarg-full-width wp-post-image' ) {
						$image = $img->item($i)->getAttribute('src');
					}
				}

				// Get Content
				$xpath = new DOMXPath($dom);
				$divContent = $xpath->query('//div[@class="entry-content"]');
				$content = $dom->saveXML( $divContent->item(0) );

				// Get Timestamp
				$time = $dom->getElementsByTagName('time');
				for ($i = 0; $i < $time->length; $i++) {
					$t = $time->item($i)->getAttribute('datetime');
					$time = date("Y-m-d H:i:m", strtotime($t));
				}
				
				// Save post to database
				@$insert = $this->savePostToDb($title, $content, $image, $time);

			}
		}

	}

	private function getPostsLinks() {

		$dom = new DOMDocument();
		$links = array();
		$page = 1;
		while ($page != -1) {
			$url = 'https://dev98.de/page/'.$page.'/';
			$urlExist = @file_get_contents($url);
			if( $urlExist ) {
				$html = file_get_contents($url);
				@$dom->loadHTML($html);
				$a = $dom->getElementsByTagName('a');				
				for ($i = 0; $i < $a->length; $i++) {
					if ( $a->item($i)->getAttribute('class') == 'btn btn-default' ) {
						$links[] = $a->item($i)->getAttribute('href');
					}
				}
				$page += 1;
			} else {
				$page = -1;
				continue;
			}
		}
		return $links;
	}

	private function uploadImage($imageUrl) {
		
		$data = file_get_contents($imageUrl);
		$target_dir = dirname(__FILE__, 2).'/resources/uploads/';
		$fileName = time().basename($imageUrl);
		$target_file = $target_dir . basename($fileName);
		$check = getimagesize($imageUrl);
		file_put_contents($target_file, $data);
		if($check !== false) {
		  return $fileName;
		} else {
		  return false;
		}

	}

	private function savePostToDb($title=false, $content=false, $imageUrl=false, $time=false) {
		$arg = array(
			'title' => $title ? $title : null,
			'content' => $content ? $content : null,
			'time' => $time ? $time : null
		);
		if ($imageUrl) {
			$imageName = $this->uploadImage($imageUrl);
			if ($imageName) {
				$arg['featuredImage'] = $imageName;
				return $this->modelPost->create($arg);
			} else {
				return $this->modelPost->create($arg);
			}
		} else {
			return $this->modelPost->create($arg);
		}
	}

}


?>