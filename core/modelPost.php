<?php

require_once('dbconnection.php');

/**
 * CRUD Class for create, read, update and delete post (OOP)
 */
class postModel
{
	
	private $id;
	private $title;
	private $content;
	private $featuredImage;
	private $timestamp;
	private $created;

	private $dbtable = 'posts';

	function __construct() {
	}

	public function create($arg=array()) {
		
		$this->title = $arg['title'] ?? 'Untitled';
		$this->content = $arg['content'] ?? NULL;
		$this->featuredImage = $arg['featuredImage'] ?? NULL;
		$this->timestamp = $arg['time'] ?? NULL;

		$stmt = DB::run("INSERT INTO `".$this->dbtable."` (`title`, `created`, `content`, `featured_image` ) VALUES (?, ?, ?, ?)", array(
			$this->title,
			$this->timestamp,
			$this->content,
			$this->featuredImage
			)
		);

		if ($stmt) {
			return $stmt->rowCount() ? true : false;
		} else {
			return false;
		}
		
	}

	public function readById($id=false) {

		if(!$id) { return false; }

		$stmt = DB::run("SELECT * FROM `".$this->dbtable."` WHERE id = ?", array($id) );

		if ($stmt) {
			return $stmt->fetch();
		} else {
			return false;
		}

	}

	public function readByTitle($title=false) {

		if(!$title) { return false; }

		$stmt = DB::run("SELECT * FROM `".$this->dbtable."` WHERE title = ?", array($title) );

		if ($stmt) {
			return $stmt->fetch();
		} else {
			return false;
		}

	}

	public function update($id=false, $arg=array() ) {
		
		if(!$id) { return false; }
		$this->title = $arg['title'] ?? 'Untitled';
		$this->content = $arg['content'] ?? NULL;
		$this->featuredImage = $arg['featuredImage'] ?? NULL;

		$stmt = DB::run("UPDATE `".$this->dbtable."` SET `title` = ?, `content` = ?, `featured_image` = ? WHERE `id` = ?", array(
			$this->title,
			$this->content,
			$this->featuredImage,
			$id
			)
		);

		if ($stmt) {
			return $stmt->rowCount() ? true : false;
		} else {
			return false;
		}
		

	}

	public function delete($id=false) {
		
		if(!$id) { return false; }
		$stmt = DB::run("DELETE FROM `".$this->dbtable."` WHERE id = ?", [$id]);

		if ($stmt) {
			return $stmt->rowCount() ? true : false;
		} else {
			return false;
		}

	}


/*
 * $page: number of the page
 * GET ALL POSTS (Functional Programming principle)
 */
	public function postPagination($page=1) {

		$p = (int)$page > 0 ? (int)$page : 0;
		$offset = ($p-1) * 6;

		$stmt = DB::run("SELECT *, (case when featured_image != 'NULL' then featured_image else 'noImage.jpg' end) as featuredImage FROM `".$this->dbtable."`  ORDER BY `created` DESC LIMIT 6 OFFSET ?", [$offset]);

		$count = DB::run("SELECT id FROM `".$this->dbtable."`");
		$allPosts = $count->rowCount();
		$nextPage = ( ($allPosts - (6*$p)) > 0 ) ? $p+1 : null;
		$prevPage = ( $p == 1 ) ? null : $p-1;
		$postsData = $stmt->fetchAll();
		$error404 = ( $p != 1 AND count($postsData) <= 0 ) ? true : false;

		$results = array(
			'posts'=> $postsData,
			'page' => $p,
			'nextPage' => $nextPage,
			'prevPage' => $prevPage,
			'error404' => $error404
		);

		// var_dump($results); die;
		return $results;
	}

}


?>