<?php
define('DB_HOST', 'vmi575481.contaboserver.net');
define('DB_NAME', 'c1netz98db');
define('DB_USER', 'c1netz98');
define('DB_PASS', 'huLYZbptD7@F');
define('DB_CHAR', 'utf8mb4');


class DB
{
    protected static $instance = null;
    protected function __construct() {}
    protected function __clone() {}

    public static function instance()
    {
        if (self::$instance === null)
        {
            $opt  = array(
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => FALSE,
            );
            $dsn = 'mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset='.DB_CHAR;
            self::$instance = new PDO($dsn, DB_USER, DB_PASS, $opt);
        }
        return self::$instance;
    }

    public static function __callStatic($method, $args)
    {
        return call_user_func_array(array(self::instance(), $method), $args);
    }

    public static function run($sql, $args = [])
    {
        if (!$args)
        {
             return self::instance()->query($sql);
        }
        
        try {
            $stmt = self::instance()->prepare($sql);
            $stmt->execute($args);
        } catch (Exception $e) {
            return false;
        }
        
        return $stmt;
    }
}