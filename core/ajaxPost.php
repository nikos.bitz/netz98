<?php
require_once('modelPost.php');
require_once('importer.php');

/**
 * Ajax calls
 */

if( !($_SERVER['REQUEST_METHOD'] === 'POST') ) {
	die('Error 500');
}

$action = $_POST['action'] ?? false;
switch ($action) {
	case 'postPagination':
		getPostsFromDev98();
		break;
	case 'importFromNetz':
		getPostsFromDev98();
		break;
	
	default:
	die('Error 500');
		break;
}


function getPostsFromDev98() {

	$postImporter = new postImporter;
	$postImporter->getPostsFromDev98();
	echo json_encode(array(
		'error' => false
	));
	die;
}


?>