<!DOCTYPE html>
<?php
require_once('./core/importer.php');
$pc = new postImporter;
?>
<html>
<head>
<?php	require_once('./header.php'); ?>
	<meta charset="utf-8">
	<title>netz98</title>
</head>
<body>

	<div class="container">
		<div class="row py-5">
		  <div class="col-12 text-center">
		    <h1 class="display-4 fw-bold">netz98</h1>
		    <p class="lead mb-4">Consume the news feed of our developer blog (https://dev98.de/).<br />Then display the data in any form. Try to bring in all your skills.</p>
		  </div>
		</div>
	  <div class="row">
	    <div class="col-4 text-center">
	      <a href="publishPost.php" class="btn btn-lg btn-primary">New post</a>
	    </div>
	    <div class="col-4 text-center">
	      <a href="allPosts.php" class="btn btn-lg btn-primary">All posts</a>
	    </div>
	    <div class="col-4 text-center">
	      <button onclick="importFromNetz()" id="importFromNetzBtn" type="button" class="btn btn-lg btn-secondary">Import from dev98.de</button>
	    </div>
	  </div>
	</div>

	<div id="loading" class="d-none">
		<div class="d-flex justify-content-center pt-5">
		  <div class="spinner-grow" role="status">
		    <span class="visually-hidden">Loading...</span>
		  </div>	  
		</div>
		<div class="d-flex justify-content-center pt-2">
			<p class="text-center">Please wait while the posts are stored in the database. It normally takes a few minutes.<br>You will be automatically redirected to the posts page.</p>
		</div>
	</div>
	
<?php	require_once('./footer.php'); ?>

<script>
	function importFromNetz() {

	  $.ajax({
	      type:"POST",
	      url: './core/ajaxPost.php',
	      dataType: 'JSON',
	      data: {action: 'importFromNetz'},
	      beforeSend: function() { 
	      	$('#loading').toggleClass('d-none')
			$('#importFromNetzBtn').css({
			    "pointer-events": "none"
			});
	       }, //LOADING
	      complete: function() {
	      	$('#loading').toggleClass('d-none')
	      	$('#importFromNetzBtn').css({
			    "pointer-events": "all"
			});
	      }, //STOP LOADING
	      success: function(response) {
	          window.location.href = "allPosts.php";
	      },
	      error: function(e) {
	          console.log('ERROR');
	      }
	  });

	}

</script>
</body>
</html>